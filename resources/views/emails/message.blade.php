<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mensaje Recibido</title>
</head>
<body>
    <div style="text-align: center">
        <img src="https://i.ibb.co/CQ0FS7f/logo.png" alt="">
        <p><strong>Recibiste un mensaje de:</strong> </p>  
        <p>{{$msg['nombres']}}</p>
        <p><strong>Apellidos:</strong> </p>
        <p>{{$msg['apellidos']}} </p>
        <p><strong>E-mail:</strong></p>  
        <p>{{$msg['email']}}  </p>
        <p><strong>Contenido:</strong></p>
        <p>{{$msg['body']}}</p>
    </div>
</body>
</html>


{{-- <a href="https://imgbb.com/"><img src="https://i.ibb.co/CQ0FS7f/logo.png" alt="logo" border="0"></a> --}}