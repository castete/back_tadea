FROM php:7.4-fpm

ARG user
ARG uid

RUN apt-get update && apt-get install -y \
	libicu-dev \
	git \
	curl \
	libpng-dev \
	libonig-dev \
	libxml2-dev \
	libzip-dev \
#	zip \
	unzip

RUN apt-get clean && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-configure zip
RUN docker-php-ext-install zip
RUN docker-php-ext-install pdo pdo_mysql intl mbstring exif pcntl bcmath gd

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user
RUN chown -R www-data:www-data /var/www/html
WORKDIR /var/www/html
COPY composer.json /var/www/html

ADD ./docker/custom-php.ini /usr/local/etc/php/conf.d/custom-php.ini
#RUN composer install
#RUN php artisan config:cache
#RUN php artisan route:cache
USER $user
