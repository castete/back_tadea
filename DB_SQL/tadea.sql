/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50172
 Source Host           : 192.168.0.25:3306
 Source Schema         : tadea

 Target Server Type    : MySQL
 Target Server Version : 50172
 File Encoding         : 65001

 Date: 25/01/2022 21:17:08
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for reconocimiento
-- ----------------------------
DROP TABLE IF EXISTS `reconocimiento`;
CREATE TABLE `reconocimiento`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text  NULL,
  `descripcion` text  NULL,
  `imagen` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
);

-- ----------------------------
-- Records of reconocimiento
-- ----------------------------
INSERT INTO `reconocimiento` VALUES (1, 'RECONOCIMIENTO', 'DESCRIPCION DEL RECONOCIMIENTO', 'IMAGEN', NULL, '2022-01-25 21:12:25');
INSERT INTO `reconocimiento` VALUES (2, 'asd', 'asd', 'asd', NULL, '2022-01-25 21:15:14');

-- ----------------------------
-- Table structure for servicio
-- ----------------------------
DROP TABLE IF EXISTS `servicio`;
CREATE TABLE `servicio`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text  NULL,
  `descripcion` text  NULL,
  `imagen` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
);

-- ----------------------------
-- Records of servicio
-- ----------------------------
INSERT INTO `servicio` VALUES (1, 'servicio', 'descripcion del servicio', 'imagensadasd', '0000-00-00 00:00:00', '2022-01-25 20:41:42');
INSERT INTO `servicio` VALUES (2, 'asd', 'asd', 'asd', '0000-00-00 00:00:00', '2022-01-25 21:15:22');

SET FOREIGN_KEY_CHECKS = 1;
